package com.shpp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Hello {
    private static final String SYS_PARAM = "format";
    private static final String PROPERTIES_FILE = "my_app.properties";
    private static final String FILE_NAME = "message";
    private static final String PATH_DIR_RESOURCE = "src" + File.separator + "main"
            + File.separator + "resources" + File.separator + PROPERTIES_FILE;
    private static final String PATH_DIR_RES = "res" + File.separator + PROPERTIES_FILE;
    private static final Logger logger = LoggerFactory.getLogger(Hello.class);

    public static void main(String[] args) {
        logger.info("\nThis is logger speaking: Hello World!\n");
        createFile(getProperties(new Properties()));
    }

    private static Properties getProperties(Properties properties) {
        try {
            if (new File(PATH_DIR_RESOURCE).isFile()) {
                InputStream in = new FileInputStream(PATH_DIR_RESOURCE);
                readFile1(properties, in);
            } else if (new File(PATH_DIR_RES).isFile()) {
                InputStream in = new FileInputStream(PATH_DIR_RES);
                readFile1(properties, in);
            } else {
                InputStream in = Hello.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
                if (in != null)
                    readFile1(properties, in);
                else logger.error("\nProperties-file was not found...\n");
            }
        } catch (Exception e) {
            makeStackTraceMessage(e);
        }
        return properties;
    }

    private static void readFile1(Properties properties, InputStream in) {
        try {
            InputStreamReader inReader = new InputStreamReader(in, StandardCharsets.UTF_8);
            BufferedReader buff = new BufferedReader(inReader);
            properties.load(buff);
            logger.info("\nThis is properties-file speaking: " + properties.getProperty("mess") + " "
                    + properties.getProperty("name") + "\n");
        } catch (Exception e) {
            makeStackTraceMessage(e);
        }
    }

    private static void createFile(Properties properties) {
            String extension = getFileExtension();
        if (!properties.isEmpty()) {
            Welcome greeting = new Welcome(properties.getProperty("mess"), properties.getProperty("name"));
            try {
                logger.info("\nBegin of creating file " + FILE_NAME + extension + "\n");
                if (extension.equals(".json")) {
                    ObjectMapper objectMapper = new ObjectMapper();
                    objectMapper.writeValue(new File(FILE_NAME + extension), greeting);
                } else {
                    XmlMapper objectMapper = new XmlMapper();
                    objectMapper.writeValue(new File(FILE_NAME + extension), greeting);
                }
                logger.info("\n" + FILE_NAME + extension + " has been successfully created.\n");
            } catch (Exception e) {
                makeStackTraceMessage(e);
            }
        }else logger.error("\n" + FILE_NAME + extension + " was not created.\n");
    }

    private static String getFileExtension() {
        String extension = ".json";
        if (System.getProperty(SYS_PARAM) == null || !System.getProperty(SYS_PARAM).equals("json")) {
            logger.info("\nSystem parameter equals .xml\n");
            extension = ".xml";
        } else logger.info("\nSystem parameter equals .json\n");
        return extension;
    }

    private static void makeStackTraceMessage(Exception e) {
        logger.error(e.getMessage());
        StackTraceElement[] stack = e.getStackTrace();
        StringBuilder exception = new StringBuilder();
        for (StackTraceElement s : stack) {
            exception.append(s.toString()).append("\n\t\t");
        }
        logger.error(exception.toString());
    }
}
