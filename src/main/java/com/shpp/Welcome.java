package com.shpp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Welcome {
    @JsonProperty("message")
    String message;
    @JsonProperty("name")
    String name;

    Welcome(String message, String name){
        this.message = message;
        this.name = name;
    }
}
